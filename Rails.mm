<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1374871856828" ID="ID_1750783662" MODIFIED="1375645920851" TEXT="vim">
<node CREATED="1374871955566" HGAP="90" ID="ID_1264353848" MODIFIED="1375105027312" POSITION="right" TEXT="1. start Project" VSHIFT="8">
<node CREATED="1374872089099" ID="ID_1918410245" MODIFIED="1374872097062" TEXT="create a directory"/>
<node CREATED="1374872100120" ID="ID_238851818" MODIFIED="1374872177413" TEXT="create a rails project (ps option)"/>
<node CREATED="1374872387395" ID="ID_680882404" MODIFIED="1374872682982" TEXT="Configure Gemfile"/>
<node CREATED="1374872140845" ID="ID_1252383965" MODIFIED="1374872682981" TEXT="configure git/github/heroku"/>
</node>
<node CREATED="1374872829801" HGAP="100" ID="ID_30248887" MODIFIED="1375105023936" POSITION="right" TEXT="2. Static Pages:" VSHIFT="3">
<node CREATED="1374873611130" ID="ID_1179140637" MODIFIED="1375645933088" TEXT="Testes: rspec" VSHIFT="-8">
<node CREATED="1374874682979" ID="ID_1068552300" MODIFIED="1374874829666" TEXT="Gemfile &apos;rspec-rails&apos;; bundle update e install"/>
<node CREATED="1374874748274" ID="ID_580054532" MODIFIED="1374874842143" TEXT="rails generate rspec:install"/>
<node CREATED="1374874932937" ID="ID_1478180718" MODIFIED="1374874992814" TEXT="Apos controllers, usar /spec/requests/static_pages_spec.rb"/>
<node CREATED="1375645937489" ID="ID_698181903" MODIFIED="1375645944088" TEXT="gem para testes: Capybara"/>
</node>
<node CREATED="1374873580202" ID="ID_1345502567" MODIFIED="1374874907899" TEXT="Criar Controller" VSHIFT="4">
<node CREATED="1374873644298" ID="ID_1873543144" MODIFIED="1374874479744" TEXT="app/controllers/static_pages_controller.rb (controler)"/>
<node CREATED="1374873648042" ID="ID_527401948" MODIFIED="1374874507825" TEXT="config/routes.rb (rota para as paginas, ex.:get home.html)"/>
<node CREATED="1374874514188" FOLDED="true" ID="ID_1230847279" MODIFIED="1374874651872" TEXT="app/views/static_pages (.erb com as views de cada p&#xe1;gina)">
<node CREATED="1374874621587" ID="ID_1594129230" MODIFIED="1374874649809" TEXT="Importam: app/views/layouts (.erb tamb&#xe9;m)"/>
</node>
<node CREATED="1375122755944" ID="ID_392783778" MODIFIED="1375122766865" TEXT="(para criar os testes de integra&#xe7;&#xe3;o) rails generate integration_test"/>
<node CREATED="1375105061110" ID="ID_1512378448" MODIFIED="1375105073099" TEXT="obs.: O controler j&#xe1; cria o diret&#xf3;rio de views"/>
</node>
</node>
<node CREATED="1375105035915" HGAP="51" ID="ID_1367316154" MODIFIED="1375112600853" POSITION="right" TEXT="3. Layout Bootstrap" VSHIFT="-1">
<node CREATED="1375105095235" ID="ID_407264850" MODIFIED="1375105149731" TEXT=".erb: &lt;xxx class: &gt; associa as classes com alguma coisa"/>
<node CREATED="1375105152732" ID="ID_1658801150" MODIFIED="1375105309866" TEXT="diret&#xf3;rio com imagens= app/assets/images/  "/>
<node CREATED="1375105311403" ID="ID_678820128" MODIFIED="1375105374889" TEXT="config bootstrap --&gt;Gemfile com&gt;  gem &apos;bootstrap-sass&apos;, &apos;2.3.2.0&apos;; bundle install"/>
<node CREATED="1375105375690" ID="ID_602164025" MODIFIED="1375105454457" TEXT="config/application.rb &quot;Adding a line for asset pipeline compatibility&quot; (listing 5.4)"/>
<node CREATED="1375105507569" ID="ID_1609341524" MODIFIED="1375105520240" TEXT="CSS: app/assets/stylesheets/custom.css.scss">
<node CREATED="1375105793142" ID="ID_462850672" MODIFIED="1375105814118" TEXT="@import &quot;bootstrap&quot;;"/>
<node CREATED="1375105814960" ID="ID_432480059" MODIFIED="1375105826045" TEXT="Esta linha ir&#xe1; carregar o framework do bootstrap"/>
</node>
<node CREATED="1375105758248" ID="ID_334366440" MODIFIED="1375106757858" TEXT="partials app/layout/*.erb">
<node CREATED="1375106759001" ID="ID_1094169956" MODIFIED="1375106771918" TEXT="Em vez de um unico application.erb, usar v&#xe1;rios"/>
<node CREATED="1375106773041" ID="ID_1955091450" MODIFIED="1375106871340" TEXT="no application.erb, usar &lt;%= render &apos;layouts/name&apos; %&gt; para carregar/renderizar o arquivo _name.html.erb naquele ponto"/>
<node CREATED="1375106873657" ID="ID_678038701" MODIFIED="1375106911436" TEXT="Criar v&#xe1;rios _name.html.erb para header, tittle bar, foot etc"/>
</node>
<node CREATED="1375112602980" ID="ID_1030362880" MODIFIED="1375112609937" TEXT="Assets Pipeline">
<node CREATED="1375112948793" ID="ID_1429224129" MODIFIED="1375112962172" TEXT="app/assets: assets specific to the present application "/>
<node CREATED="1375112963409" ID="ID_988051865" MODIFIED="1375112971038" TEXT="lib/assets: assets for libraries written by your dev team "/>
<node CREATED="1375112972201" ID="ID_1809541142" MODIFIED="1375112973270" TEXT="vendor/assets: assets from third-party vendors "/>
<node CREATED="1375113699906" FOLDED="true" ID="ID_1684013401" MODIFIED="1375114957865" TEXT=".scss for Sass, .coffee for CoffeeScript, and .erb.  --&gt; coffscript compiles a javascript">
<node CREATED="1375113734251" ID="ID_771841885" MODIFIED="1375113735208" TEXT="http://railscasts.com/episodes/267-coffeescript-basics"/>
</node>
<node CREATED="1375114959683" FOLDED="true" ID="ID_1229217067" MODIFIED="1375116709196" TEXT="LESS values for bootstrap (colors, and so on): http://bootstrapdocs.com/v2.0.4/docs/less.html">
<node CREATED="1375114982803" ID="ID_681452368" MODIFIED="1375115002272" TEXT="Use like custom variable on scss asset file"/>
<node CREATED="1375115003010" ID="ID_1631193756" MODIFIED="1375115032801" TEXT="in the css, use the simbol $variable_name to invoke (forget the @ simbol)"/>
</node>
<node CREATED="1375116710712" ID="ID_142215583" MODIFIED="1375116741454" TEXT="rails routes.rb to use page_path instead of &quot;/static_pages/pages&quot;"/>
</node>
</node>
<node CREATED="1375670683088" ID="ID_884516950" MODIFIED="1375670696733" POSITION="left" TEXT="Tratamento de erros - mostrando bonito na pagina - capitulo 7"/>
<node CREATED="1375665191647" FOLDED="true" ID="ID_26862154" MODIFIED="1375670679407" POSITION="left" TEXT="Rails 4 x Rails 3">
<node CREATED="1375665198922" ID="ID_1072645609" MODIFIED="1375665217327" TEXT="Verificar atributos passados de forma segura">
<node CREATED="1375665219311" ID="ID_1750338417" MODIFIED="1375665230916" TEXT="Rails 3: attr_accessible"/>
<node CREATED="1375665231544" ID="ID_1319205177" MODIFIED="1375665381817" TEXT="rails 4: strong parameters no controler  (chap 7.3)"/>
</node>
</node>
<node CREATED="1375290142033" FOLDED="true" ID="ID_1321362939" MODIFIED="1375665188710" POSITION="left" TEXT="Models">
<node CREATED="1375290157018" ID="ID_1726163847" MODIFIED="1375290204190" TEXT="rails generate model User name:string email:string"/>
<node CREATED="1375290220434" ID="ID_1742886085" MODIFIED="1375290237346" TEXT="j&#xe1; vai criar um arquivo default de teste (n&#xe3;o tem o no-test-framwork)"/>
<node CREATED="1375290254481" ID="ID_1190929264" MODIFIED="1375290536575" TEXT="&quot;rake db:migrate&quot; para adicionar os modelos"/>
<node CREATED="1375290264529" ID="ID_1674849287" MODIFIED="1375290543195" TEXT="(j&#xe1; considera que em algum momento foi dado &quot;rake db:create all&quot; caso use postgres para criar as tabelas de acordo com o arquivo database.yml"/>
<node CREATED="1375290295161" ID="ID_642998903" MODIFIED="1375290410080" TEXT="rake db:rollback no caso de algo errado"/>
<node CREATED="1375290416768" ID="ID_651171247" MODIFIED="1375290435921" TEXT="app/models = modelos ; spec/models = testes!"/>
<node CREATED="1375290498768" ID="ID_1429216796" MODIFIED="1375290523793" TEXT="&quot;rails console --sandbox&quot;  pode ateh fazer altera&#xe7;&#xf5;es no banco que ser&#xe1; feito rollback"/>
<node CREATED="1375290606791" ID="ID_303060228" MODIFIED="1375290832918" TEXT="uniqueness: precisa validar tanto na entrada quando no banco (ver the &quot;The uniqueness caveat&quot;)"/>
</node>
<node CREATED="1375291245802" FOLDED="true" ID="ID_557371068" MODIFIED="1375630072284" POSITION="left" TEXT="Security">
<node CREATED="1375291252522" ID="ID_1555272915" MODIFIED="1375291266626" TEXT="gem &apos;bcrypt-ruby&apos;, &apos;3.0.1&apos;    Para passwords"/>
<node CREATED="1375291414249" ID="ID_1618587214" MODIFIED="1375291431992" TEXT="&quot;:password_digest&quot;    --&gt;coluna obrigat&#xf3;ria"/>
<node CREATED="1375291461655" ID="ID_1065924375" MODIFIED="1375291463048" TEXT="rails generate migration add_password_digest_to_users password_digest:string"/>
</node>
<node CREATED="1375630073143" FOLDED="true" ID="ID_488874040" MODIFIED="1375630303276" POSITION="left" TEXT="Velocidade nos testes">
<node CREATED="1375630081817" ID="ID_1332184199" MODIFIED="1375630113537" TEXT="Configurar &quot;config/environments/test.rb&quot; com &quot;ActiveModel::SecurePassword.min_cost = true&quot;"/>
<node CREATED="1375630094781" ID="ID_221219747" MODIFIED="1375630137586" TEXT="O comando acima ir&#xe1; dimibuir a criptografia das senhas apenas no ambiente de teste"/>
</node>
<node CREATED="1375630304920" ID="ID_1754763223" MODIFIED="1375630324858" POSITION="left" TEXT="Custom images:  Paperclip or CarrierWave (ver notas no capitulo 7)"/>
</node>
</map>
