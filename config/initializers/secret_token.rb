# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SampleApp::Application.config.secret_key_base = 'f5445d372360cfbb011f98c8ea310f2ebc481aa87bd3488b43968f2d3f3132993e18c94bd079ef33e3575e9d3f5df67b8e619f81b7e3926c80936f10214de0b8'
